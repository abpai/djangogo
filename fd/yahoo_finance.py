# www.appannie.com

import sys
import os, os.path
import time
import random
import json
# import requesocks as requests
import requests
from bs4 import BeautifulSoup
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apps.settings')
from django.utils import timezone
# from fd.models import XXXX
# BASE_DIR = os.path.dirname(__file__)
# IMAGES_DIR = os.path.join(BASE_DIR, 'static/images')
# print 'Images will be saved to:' + IMAGES_DIR

# request header
headers_choices = [
    {'Host': 'finance.yahoo.com',
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
    'Referer': 'http://finance.yahoo.com/q?s=dis',
    'Accept-Encoding': 'gzip,deflate,sdch',
    'Accept-Language': 'en-US,en;q=0.8'}
    ]

proxies = {'http': 'socks5://127.0.0.1:9150',
           'https': 'socks5://127.0.0.1:9150'}

class YahooFinance(object):
    """docstring for YahooFinance Crawler"""
    def __init__(self, ticker):
        self.ticker = ticker.upper()
        self.base_url = 'http://finance.yahoo.com/'
        self.key_statistics = self.base_url+'q/ks?s='+ticker+'+Key+Statistics'
        self.table_selector = 'yfnc_datamodoutline1'


    def get_data(self, metric='', return_json=False):
        page = requests.get(self.key_statistics, headers=headers_choices[0])
        soup = BeautifulSoup(page.content)
        table = soup.find_all('table', {'class': self.table_selector})
        data_grab = {'price': soup.find(id='yfs_l84_'+self.ticker.lower()).text,
                     'market-cap': table[0].find_all('td')[2].text,
                     'ev': table[0].find_all('td')[4].text,
                     'pe': table[0].find_all('td')[6].text,
                     'fpe': table[0].find_all('td')[8].text,
                     'peg-ratio': table[0].find_all('td')[10].text,
                     'price-sales': table[0].find_all('td')[12].text,
                     'price-book': table[0].find_all('td')[14].text,
                     'ev-revenue': table[0].find_all('td')[16].text,
                     'ev-ebitda': table[0].find_all('td')[18].text,
                     'revenue': table[4].find_all('td')[3].text,
                     'gp': table[4].find_all('td')[9].text,
                     'ebitda': table[4].find_all('td')[11].text,
                     'ni': table[4].find_all('td')[13].text,
                     'eps': table[4].find_all('td')[15].text,
                     'qt-rev-growth': table[4].find_all('td')[17].text,
                     'ocf': table[6].find_all('td')[3].text,
                     'lfcf': table[6].find_all('td')[5].text,
                     'beta': table[7].find_all('td')[3].text,
                     'high': table[7].find_all('td')[9].text,
                     'low': table[7].find_all('td')[11].text,
                     }
        if return_json==False:
            return data_grab[metric]
        else:
            return json.dumps(data_grab, ensure_ascii=False, indent=4)

# if __name__ == '__main__':
#     ticker = raw_input('Enter Stock Ticker : ')
#     return_json = raw_input('Enter Metric (leave blank for all data) : ')
#     stock = YahooFinance(ticker)
#     if return_json == '':
#         print stock.get_data(return_json=True)
#     else:
#         print stock.get_data(metric=return_json)



# dis = YahooFinance('tsla')
# print dis.get_data('ev-ebitda')


