from django.conf.urls import patterns, url
from fd import views

urlpatterns = patterns('',
    url(r'^(?P<ticker>\w+)/$', views.index, name='index'),
    url(r'^(?P<ticker>\w+)/(?P<metric>[^/]+)/$', views.metric, name='metric'))