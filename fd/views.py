from django.shortcuts import render
from django.http import HttpResponse
from yahoo_finance import YahooFinance


# Create your views here.
def index(request, ticker):
    stock = YahooFinance(ticker)
    data = stock.get_data(ticker, return_json=True)
    return HttpResponse(data)

def metric(request, ticker, metric):
    stock = YahooFinance(ticker)    
    data = stock.get_data(metric)
    return HttpResponse(data)
