from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.
def base(request):
    context = RequestContext(request)

    context_dict = {}
    return render_to_response('abp/base.html', context_dict, context)
