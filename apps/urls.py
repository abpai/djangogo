from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from apps import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.base, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^abp/', include('abp.urls')),
    url(r'^fd/', include('fd.urls')),
)
